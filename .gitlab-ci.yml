# SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
# SPDX-License-Identifier: MIT

image: python:3.9

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

cache:
  paths:
    - .cache/pip
    - .venv
  key: "${CI_JOB_NAME}"

stages:
  - test
  - audit
  - package
  - acceptance

.install-deps-template: &install-deps
  before_script:
    - pip install poetry
    - poetry --version
    - poetry config virtualenvs.in-project true
    - poetry install -vv

test:
  <<: *install-deps
  stage: test
  script:
    - make test
  tags:
    - docker
  rules:
    - if: '$CI_COMMIT_TAG'
      when: never
    - if: '$CI_COMMIT_BRANCH'
      when: always
  artifacts:
    reports:
      junit: build/tests.xml

.audit-template: &audit
  <<: *install-deps
  stage: audit
  tags:
    - docker
  rules:
    - if: '$CI_COMMIT_TAG'
      when: never
    - if: '$CI_COMMIT_BRANCH'
      when: always

check-code:
  <<: *audit
  script:
    - make check-code
    - make check-formatting
  artifacts:
    when: on_failure
    expose_as: "Linter Report"
    paths: ["build/flake8.txt"]

check-docs:
  <<: *audit
  script:
    - make docs
  artifacts:
    expose_as: "Sphinx Docs"
    paths: ["docs/html/"]

check-coverage:
  <<: *audit
  script:
    - make check-coverage
  coverage: '/^TOTAL.+?(\d+\%)$/'
  artifacts:
    expose_as: "Coverage Report"
    paths: ["build/htmlcov/"]

check-license-metadata:
  <<: *audit
  script:
    - make check-license-metadata

check-security:
  <<: *audit
  script:
    - make check-security

.package-template: &package
  <<: *install-deps
  stage: package
  script:
    - make package
    - poetry config repositories.gitlab "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi"
    - poetry publish --repository gitlab -u gitlab-ci-token -p ${CI_JOB_TOKEN}
  tags:
    - docker

snapshot:
  <<: *package
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'

release:
  variables:
    RELEASE: "true"
  <<: *package
  rules:
    - if: '$CI_COMMIT_TAG =~ /^\d+.\d+.\d+$/'
  artifacts:
    paths:
      - docs/html

pages:
  stage: package
  script:
    - mv docs/html public
  tags:
    - docker
  rules:
    - if: '$CI_COMMIT_TAG =~ /^\d+.\d+.\d+$/'
  needs:
    - release
  artifacts:
    paths:
    - public

smoke-test-snapshot:
  stage: acceptance
  script:
    - pip install poetry
    - version=$(poetry version -s)-rc+$(git rev-parse --short HEAD)
    - pip install sample-calculator==${version} --index-url https://gitlab-ci-token:${CI_JOB_TOKEN}@${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/packages/pypi/simple 
    - sample-calculator <<< "1;2;3"
  tags:
    - docker
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'
  needs:
    - snapshot

smoke-test-release:
  stage: acceptance
  script:
    - pip install poetry
    - version=$(poetry version -s)
    - pip install sample-calculator==${version} --index-url https://gitlab-ci-token:${CI_JOB_TOKEN}@${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/packages/pypi/simple 
    - sample-calculator <<< "1;2;3"
  tags:
    - docker
  rules:
    - if: '$CI_COMMIT_TAG =~ /^\d+.\d+.\d+$/'
  needs:
    - release

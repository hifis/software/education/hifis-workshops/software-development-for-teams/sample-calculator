Fix #<Add issue ID>

## Reviewer Checklist

- [ ] Is the purpose and scope of the change clear?
- [ ] Are defined acceptance criteria met?
- [ ] Are the code changes sound and readable?
- [ ] Is sufficient documentation provided (comments, user / developer guide)?
- [ ] Is the license of newly added dependencies compatible with our license?
- [ ] Have been sufficient automated tests provided (code coverage)?
- [ ] Are the Git commit(s) (messages) useful?

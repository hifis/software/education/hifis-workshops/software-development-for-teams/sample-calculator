# SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
# SPDX-License-Identifier: MIT

ifeq ($(origin RELEASE), undefined)
SAMPLE_CALCULATOR_VERSION = $(shell poetry version -s)-rc+$(shell git rev-parse --short HEAD)
else
SAMPLE_CALCULATOR_VERSION = $(shell poetry version -s)
endif

all: audit

clean:
	rm -rf build
	rm -rf dist
	rm -rf docs/html/
	poetry run coverage erase

prepare:
	mkdir -p build

test: prepare
	poetry run pytest --junit-xml=build/tests.xml tests/

formatting:
	poetry run black .
	poetry run isort .

check-code: prepare
	poetry run flake8 sample_calculator tests --exit-zero
	poetry run flake8 sample_calculator tests --output-file=build/flake8.txt

check-coverage: prepare
	poetry run pytest --cov=sample_calculator --cov-fail-under=80 --cov-report=term-missing --cov-report=html

check-formatting:
	poetry run black sample_calculator tests --check
	poetry run isort sample_calculator tests --check-only

check-license-metadata:
	poetry run reuse lint

check-security:
	poetry run bandit -r sample_calculator/

audit: check-code check-coverage check-formatting check-license-metadata check-security

docs: prepare
	poetry run sphinx-apidoc sample_calculator --force --output-dir=docs/src/
	poetry run sphinx-build -D version="$(SAMPLE_CALCULATOR_VERSION)" -b html docs/src/ docs/html/

package: docs
	poetry version $(SAMPLE_CALCULATOR_VERSION)
	poetry build

.. SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
.. SPDX-License-Identifier: MIT


User Guide
==========

Here you can find information about how to install and use the Sample Calculator command line tool.

Installation
------------

At least you require Python >= 3.9 to run Sample Calculator.
To install it, extract the archive and perform the following steps:

.. code-block:: bash

   pip install sample-calculator

Usage
-----

.. code-block::

   sample-calculator [-h] [--filepath FILEPATH]

   Calculates average, variance, and deviation of the given sample values.

   optional arguments:

     -h, --help           show this help message and exit
     --filepath FILEPATH  Path to the file which contains the sample values.

Examples
^^^^^^^^

**Reading the sample values from command line:**

::

   sample-calculator 
   2020-05-29 11:25:09,675 - root - INFO - Insert the sample values (separated by ';'):
   1;2;3
   2020-05-29 11:25:12,825 - root - INFO - Using the following sample values: [Decimal('1'), Decimal('2'), Decimal('3')]
   2020-05-29 11:25:12,845 - root - INFO - AVERAGE: 2.00
   2020-05-29 11:25:12,845 - root - INFO - VARIANCE: 1.00
   2020-05-29 11:25:12,845 - root - INFO - DEVIATION: 1.00
   2020-05-29 11:25:12,846 - root - INFO - MEDIAN: 2.00
   2020-05-29 11:25:12,846 - root - INFO - MEDIAN_LOW: 2.00
   2020-05-29 11:25:12,846 - root - INFO - MEDIAN_HIGH: 2.00
   2020-05-29 11:25:12,846 - root - INFO - MODE_UNIQUE: nan
   2020-05-29 11:25:12,847 - root - INFO - SQUARE_SUM: 14.00
   2020-05-29 11:25:12,847 - root - INFO - PRODUCT: 6.00

**Reading the sample values from a file:**

::

   sample-calculator --filepath=sample_values.csv
   2020-05-29 11:27:18,883 - root - INFO - Using the following sample values: [Decimal('12'), Decimal('45.0'), Decimal('89.9'), Decimal('-78'), Decimal('23'), Decimal('78'), Decimal('90'), Decimal('32')]
   2020-05-29 11:27:18,890 - root - INFO - AVERAGE: 36.49
   2020-05-29 11:27:18,890 - root - INFO - VARIANCE: 3060.19
   2020-05-29 11:27:18,890 - root - INFO - DEVIATION: 55.32
   2020-05-29 11:27:18,890 - root - INFO - MEDIAN: 38.50
   2020-05-29 11:27:18,890 - root - INFO - MEDIAN_LOW: 32.00
   2020-05-29 11:27:18,890 - root - INFO - MEDIAN_HIGH: 45.00
   2020-05-29 11:27:18,891 - root - INFO - MODE_UNIQUE: nan
   2020-05-29 11:27:18,891 - root - INFO - SQUARE_SUM: 32072.01
   2020-05-29 11:27:18,892 - root - INFO - PRODUCT: -19564239951360.00

**Example input file sample_values.csv:**

::

   12;45.0;89.9;-78;
   23;78;90;32


Logging
^^^^^^^

As default all results are printed on the command line. Additionally, the results are captured in the file *sample_calculator.log*.
The file is created in the directory in which you run Sample Calculator.

You can change the logging preferences by adapting the file ``<SITE-Packages>/sample_calculator/logger.conf`` in the site-packages directory.
For details on the file format, please see `the logging module reference page <http://docs.python.org/3/library/logging.config.html#configuration-file-format>`__.

**Example of a console-only logger:**

::

   # Defines the currently used logger
   [loggers]
   keys=root
   
   # Defines list of available loggers
   [logger_root]
   handlers=console
   
   # Defines list of available handlers
   [handlers]
   keys=console
   
   # Defines list of available formatters
   [formatters]
   keys=default
   
   # Defines the console logger
   [handler_console]
   class=StreamHandler
   level=INFO
   formatter=default
   args=(sys.stdout,)
   
   # Defines the logging format
   [formatter_default]
   format=%(asctime)s - %(name)s - %(levelname)s - %(message)s

# SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
# SPDX-License-Identifier: MIT


"""
Implements the InputMethod tests.
"""


import pytest

from sample_calculator import inputs
from sample_calculator.inputs import base
from sample_calculator.inputs.inputs import console_input


def test_success():
    assert base.create_inputmethod(inputs.CONSOLE_INPUT).__class__ == console_input.ConsoleInputMethod


def test_invalid_name():
    with pytest.raises(base.InputMethodError):
        base.create_inputmethod("")


def test_incorrect_args():
    with pytest.raises(base.InputMethodError):
        base.create_inputmethod(inputs.CONSOLE_INPUT, "")
